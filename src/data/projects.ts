type Project = {
  title: string;
  description: string;
  href: string;
  repository?: string;
  image?: string;
} | null;

export { Project };

const projects: Project[] = [
  {
    title: 'Default Project 1',
    image: 'https://cdn.vuetifyjs.com/images/cards/docks.jpg',
    href: 'https://gitlab.com/',
    repository: 'https://gitlab.com/',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis ante eu tortor imperdiet lacinia ac sit amet purus.'
  },
  {
    title: 'Default Project 2',
    image: 'https://cdn.vuetifyjs.com/images/cards/docks.jpg',
    href: 'https://gitlab.com/',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis ante eu tortor imperdiet lacinia ac sit amet purus.'
  },
  {
    title: 'Default Project 3',
    href: 'https://gitlab.com/',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis ante eu tortor imperdiet lacinia ac sit amet purus.'
  }
];

export default projects;
