import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '../pages/Home.vue';
import About from '../pages/About.vue';
import Posts from '../pages/Posts.vue';
import Projects from '../pages/Projects.vue';

Vue.use(VueRouter);

const routes = [
  { path: '*', component: Home },
  { path: '/about', name: 'About', component: About },
  { path: '/posts', name: 'Posts', component: Posts },
  { path: '/projects', name: 'Projects', component: Projects }
];

export default new VueRouter({ routes });
